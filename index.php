<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Пример работы виджета</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <script type="text/javascript" src="release/catapultowidget.min.js"></script>
</head>
<body>
<div id="for_catapultowidget"></div>

<script type="text/javascript">
    window.CatapultoWidgetObject = new CatapultoWidget({
        service_path: 'service.php',
        sender_contact_params: {
            locality_id: 3615291,
            zip: 440003,
        },
        date_type: 'date',
        dadata_token: "44e4c43175933270b74b5858cfc67886c495d3c5",
        link: 'for_catapultowidget',
        cargo: {
            height: 10,
            length: 10,
            quantity: 1,
            width: 10,
            weight: 10
        },
        onSelectPvzItem: (Item, widget) => { // Событие при выборе ПВЗ варианта
        },
        onSelectCourierItem: (Item, widget) => {// Событие при выборе курьерского варианта
        },
        onRateResponse: function (RateJson, Widget) {
            console.log(RateJson);
            RateJson.results.forEach(function(item) {
            });
        },
    });
</script>
</body>
</html>











