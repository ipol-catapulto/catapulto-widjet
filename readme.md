# Официальный виджет Catapulto

*Catapulto — это сервис агрегатор курьерской доставки*

Виджет позволит вам добавить на свой сайт функционал для расчета стоимости доставки и отображения пунктов выдачи на карте.
Для работы виджета вам понадобиться **API ключ от Catapulto** и **ключ доступа к сервису Dadata**.

Чтобы получить ключ доступа Dadata зарегистрируйтесь в сервисе, перейдите в личный кабинет и в основном разделе ЛК ("Почта, ключи, деньги") вы увидите Api-ключ. Большинству магазинов хватает бесплатного лимита запросов.

## Установка

1. Для начала определитесь в каком разделе сайта у вас будут находится скрипты виджета и закачайте туда 2 скрипта из данного репозитория: `service.php` и `catapultowidget.min.js`
Примеры ниже указаны из расчета что файлы у вас загружены в корень сайта.
Файл index.php - это демонстрационный пример подключения виджета.


2. В файле service.php укажите в самом низу скрипта в параметре $token = значение вашего токена доступа к API Catapulto


3. На странице сайта где должен располагаться виджет в блоке `<head>` добавьте тег настройки отображения мобильной версии:
```html
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
```
* \*Проверьте что наличие этого тега не нарушает работу вашей текущей моб.версии сайта. И что тег viewport не присутствовал уже в коде страницы. 

4. Добавьте в блоке `<head>`, либо в любом другом месте перед вызовом самого виджета код подключения скрипта виджета:

```javascript
<script type="text/javascript" src="/src/catapultowidget.min.js"></script>
```

5. Определите на странице тег в котором будет инициализироваться виджет, например `<div id="for_catapultowidget"></div>`
 

6. Добавьте на страницу код инициализации виджета:
 
```javascript
<script type="text/javascript">
    new CatapultoWidget({
        service_path: '/service.php', // Обязательный //тут должен быть путь к месту где вы разместили файл service.php
        sender_contact_params: { // Обязательный // Параметры отправителя (вашего склада) либо через cityFrom либо через locality_id и zip
            // Параметры адреса через точное указание
            "locality_id": 3615291, //id города сформированный из запроса geo в свагере: https://devaeshop.ctplt.ru/docs/
            "zip": 440003, // индекс вашего склада
            // Или через адресс через первый результат дадаты // но этот способ работает чуть медленнее
            cityFrom : {
                // Адрес
                address: 'г Москва',
                // город для уточнения // не обязательный параметр
                city: 'Москва',
            },
        },
        date_type: 'date', // Не обязательный  // date // diff, по умолчанию diff
        dadata_token: "44e4c43175933270b74b5858cfc67886c495d3c5", // Обязательный // Токен дадаты
        link: 'for_catapultowidget', // Обязательный // ID блока (тега) куда встроится виджет
        // параметры груза
        // параметры для одной упаковки
        cargo: {
            height: 10, //габариты в см.
            length: 10,
            width: 10,
            quantity: 1,
            weight: 10 // вес в кг.
        },
        // параметры для нескольких упаковок
        // cargo: [
        //     { 
        //         height: 10, //габариты в см.
        //         length: 10,
        //         quantity: 1,
        //         width: 10,
        //         weight: 10 // вес в кг.
        //     },
        //     {
        //         height: 20, //габариты в см.
        //         length: 10,
        //         width: 10,
        //         quantity: 1,
        //         weight: 10 // вес в кг.
        //     }
        // ],
        /* Ниже указаны не обязательные параметры:
        insured_value: 1000, //страховка, по умолчанию 0. Рекомендуется ставить сумму страховки равную сумме заказа.
        need_insurance: true // Флаг страховки заказа по умолчанию false
        popup_mode: true, // режим popup добавляет кнопку закрытия
        onPopupClose: (Widget) => { // обработчик после закрытия кнопкой в режиме попапа
            console.log(Widget.isClosedPopup())
        },
        services_filter: 'NP', //NP или COD или "NP,COD"// Фильтр по варианту оплаты наложным платежом: NP - возможность приема наложного платежа за товары, COD - платеж за доставку. по умолчанию пусто
        filter_card: false, //Дополнительная фильтрация ПВЗ - принимающие оплату по карте
        filter_cash: false, //Дополнительная фильтрация ПВЗ - принимающие оплату наличными
        //Aдрес доставки по умолчанию для которого запустится расчет, можно указать 1 из 2 параметров
        location: {
            address: 'г Урюпинск', // Адрес
            city: 'Урюпинск', // Город для уточнения адресса
        },
        only_info: true, // По умолчанию false. В режиме true скрывает кнопки "выбрать" и показывает виджет как чисто информационный блок
        only_delivery_type: 'Pvz', // Courier или Pvz используется для ограничения вариантов доставки выводимых в виджете только курьерскими вариантами или только ПВЗ
        date_type: 'date', // date / diff, по умолчанию diff. Показывает даты доставки в формате "8 нояб." или "2 дня"
        delivery_type: 'door', // Не обязательный // door // warehouse // Вариант передачи заказа в службу доставки. door (по умолчанию) - забор от двери, warehouse - самостоятельная доставка на склад курьерской компании или консолидированный заборы
        day_shift: 1, //значение будет влиять на сдвиг ближашей даты доставки, по умолчанию 0
        onSelectPvzItem: (Item, widget) => { // Событие при выборе ПВЗ варианта
            console.log(Item.getData());
            alert(JSON.stringify(Item.getData(), null, 4))
        },
        onSelectCourierItem: (Item, widget) => {// Событие при выборе курьерского варианта
            console.log(Item.getData());
            alert(JSON.stringify(Item.getData(), null, 4))
        },
        onRateResponse: function (RateJson, Widget) { //пример переопределения стоимости доставки по собственной логике
			console.log(RateJson);
			RateJson.results.forEach(function(item) {
					item.price = 10; // вся стоимость доставки станет равной 10 рублей.
				});
			
		},
        onTariffResponse: function (Tariff, Widget) { // Событие получения возможной даты и времени доставки для варианта курьерской доставки с примером получения рейта для него. 
            let courierItems = Widget.getData().getCourierItems(); // варианты курьерской доставки
            let Variant = false;
            for (let i in courierItems) { 
                if (courierItems[i].id === Tariff.id) { // определение какому варианту курьерской доставки принадлежит только что полученный "тариф" (список возможных дат и времени доставки)
                    Variant = courierItems[i];
                    break;
                }
            }
            console.log(Variant);
            console.log(Tariff);
        }*/
    });
</script>
```
----


Пример вывода сообщения о выбранном варианте и перезагрузке виджета при подтверждении выбора
```javascript
    onSelectCourierItem: (Item, widget) => { // Событие при выборе ПВЗ варианта
        const shortItemData = widget.getShortDataForItem(Item);
        let titleDiv = document.createElement('div');
        titleDiv.innerHTML = shortItemData.title;
    
        let priceDiv = document.createElement('div');
        priceDiv.innerHTML = shortItemData.price;
    
        let dateDiv = document.createElement('div');
        dateDiv.innerHTML = shortItemData.date;
    
        let refreshButton = document.createElement('div');
        refreshButton.innerHTML = 'Изменить';
        refreshButton.addEventListener('click', (e) => {
            e.preventDefault();
            widget.refresh();
        })
        refreshButton.style.color = '#fff';
        refreshButton.style.backgroundColor = '#01bd6c';
        refreshButton.style.padding = '10px';
        refreshButton.style.marginTop = '20px';
        refreshButton.style.lineHeight = '1';
        refreshButton.style.textAlign = 'center';
        refreshButton.style.width = '150px';
        refreshButton.style.borderRadius = '6px';
        refreshButton.style.cursor = 'pointer';
    
        let container = document.createElement('div');
        container.style.padding = '50px';
        document.getElementById('for_catapultowidget').innerHTML = '';
        document.getElementById('for_catapultowidget').appendChild(container);
        container.appendChild(titleDiv);
        container.appendChild(priceDiv);
        container.appendChild(dateDiv);
        container.appendChild(refreshButton);
    },
    onSelectPvzItem: (Item, widget) => {// Событие при выборе курьерского варианта
        const shortItemData = widget.getShortDataForItem(Item);
    
        let titleDiv = document.createElement('div');
        titleDiv.innerHTML = shortItemData.title;
    
        let priceDiv = document.createElement('div');
        priceDiv.innerHTML = shortItemData.price;
    
        let dateDiv = document.createElement('div');
        dateDiv.innerHTML = shortItemData.date;
    
        let addressDiv = document.createElement('div');
        addressDiv.innerHTML = shortItemData.address;
    
        let refreshButton = document.createElement('div');
        refreshButton.innerHTML = 'Изменить';
        refreshButton.addEventListener('click', (e) => {
            e.preventDefault();
            widget.refresh();
        })
        refreshButton.style.color = '#fff';
        refreshButton.style.backgroundColor = '#01bd6c';
        refreshButton.style.padding = '10px';
        refreshButton.style.marginTop = '20px';
        refreshButton.style.lineHeight = '1';
        refreshButton.style.textAlign = 'center';
        refreshButton.style.width = '150px';
        refreshButton.style.borderRadius = '6px';
        refreshButton.style.cursor = 'pointer';
    
        let container = document.createElement('div');
        document.getElementById('for_catapultowidget').innerHTML = '';
        document.getElementById('for_catapultowidget').appendChild(container);
        container.appendChild(titleDiv);
        container.appendChild(priceDiv);
        container.appendChild(dateDiv);
        container.appendChild(addressDiv);
        container.appendChild(refreshButton);
    },
```
Для получения дополнительных данных виджета (например в событии выбора) можно использовать сам созданный обьект так и ссылку на него переданную в методах событий через widget.getData()
```javascript
Widget.getData();
Widget.getData().getRateParams();
Widget.getData().getSelectedCity();
```

Виджет должен появиться на странице.
Пример: https://ipol.ru/webService/catapulto/widget/
 
Если вы хотите открывать виджет во всплыв.окне, то можете добавить на своей стороне кнопку открытия виджета и крестик (кнопку) закрытия виджета. Пример открытия через всплывающее окно вы можете увидеть вот в этом файле popup.html в репозитории. 
Пример: https://ipol.ru/webService/catapulto/widget/popup.html

По нажатию на кнопку открытия виджета можно проверять, если объекта виджета еще нет, инициализировать его. Затем открывать видимость блоку <div> где должен располагаться виджет. Предварительно задать этому блоку CSS с абсолютным позиционированием.
При нажатии на кнопку закрытия виджета просто убирать видимость этому блоку.
Доступен попап мод, добавляющий кнопку закрытия в попап и событие закрытия
```javascript
popup_mode: true, // режим popup добавляет кнопку закрытия и адапртирует под неё стили
onPopupClose: (Widget) => { // обработчик после закрытия кнопкой в режиме попапа
    console.log(Widget.isClosedPopup())
}
```
Также методы для сокрытия открытия и проверки текущего состояния
```javascript
Widget.hide();
Widget.show();
Widget.isClosedPopup();
```
Также виджет имеет методы `destroy`, `refresh` и `reinitialize`

**Для корректной работы service.php необходимо php7.4 и включённый curl (extension=curl)** 

Используйте `release/catapultowidget.min.js` вместо `src/catapultowidget.js` он имеет поддержку более старых версий браузеров
`src/catapultowidget.js` необходим только для ознакомления и разработки.

В случае необходимости работы с исходниками после внесения изменений в `src/catapultowidget.js` пересоберите проект.

Для сборки в директории проекта выполните сконфигурированную команду
```shell
npm run build
```

Для работы понадобится `npm` и будет необходимо подтянуть пакеты описанные в `package.json`, выполнив команду в директории проекта
```shell
npm install
```
