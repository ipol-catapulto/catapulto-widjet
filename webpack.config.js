const path = require('path');

module.exports = {
    entry: './lib/catapultowidget.js',
    output: {
        path: path.resolve(__dirname, "release"),
        filename: './catapultowidget.min.js',
        library: 'catapultowidget'
    }
};