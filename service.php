<?php
header('Access-Control-Allow-Origin: *');
error_reporting(0);
/**
 * Catapulto API widget helper
 */
class CatapultoWidgetData
{
    /** @var string geo api path */
    private const GEO_API = 'geo';
    private const GET_ICONS = 'company-icon';
    private const TARIFF = 'tariff/';
    /** @var string create contact api path */
    private const CONTACT_API = 'contact';

    /** @var string create cargo api path */
    private const CARGO_API = 'cargo';

    /** @var string Create rate api path */
    private const RATE_API = 'rate';

    /** @var string Create terminal api path */
    private const TERMINAL_API = 'terminal';

    /** @var string rate get api path */
    private const RATE_GETTER_API = 'rate/';

    /** @var string terminal getter api path */
    private const TERMINAL_GETTER_API = 'terminal/';

    /** @var string API token */
    private $token;
    /** @var string Base Api url */
    private $apiUrl;

    /**
     * @param string $token API token
     * @param string $apiUrl Base Api Url
     */
    public function __construct(string $token, string $apiUrl = 'https://stagingeshop.ctplt.ru/')
    {
        $this->token = $token;
        $this->apiUrl = $apiUrl;
    }
    /**
     * Widget ajax listener
     * @param array $postData
     * @throws Exception
     */
    public function deliveryWidgetAction(array $postData): void
    {
        if (!isset($postData['METHOD']) || !isset($postData['PARAMS'])) {
            return;
        }
        $params = json_decode($postData['PARAMS'], true);
        switch ($postData['METHOD']) {
            case 'create_rate':
                echo json_encode($this->createRateAction($params));
                break;
            case 'get_rate':
                echo json_encode($this->getRateAction($params));
                break;
            case 'get_terminals':
                echo json_encode($this->getTerminalsAction($params));
                break;
            case 'get_terminal':
                echo json_encode($this->getTerminalAction($params));
                break;
            case 'get_tariff':
                echo json_encode($this->getTariffAction($params));
                break;
            default:
                break;
        }
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    private function getTerminalAction(array $data): array
    {
        if (empty($data['terminal_id'])) {
            throw new Exception('terminal_id empty');
        }
        $terminalId = $data['terminal_id'];
        return $this->getTerminal($terminalId);
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    private function getTariffAction(array $data): array
    {
        if (empty($data['tariff_id'])) {
            throw new Exception('tariff_id empty');
        }
        $tariffId = $data['tariff_id'];

        $filter = null;
        if ($data['pickup_days_shift'] > 0 && $data['pickup_days_shift'] <= 365) {
            $filter = [
                'pickup_days_shift' => (int) $data['pickup_days_shift'],
            ];
        }
        $tariff = $this->getTariff($tariffId, $filter);
        if (!isset($tariff[0]['id'])) {
            echo json_encode(['error' => 'CANNOT GET TARIFF']);
            die();
        }
        return $tariff;
    }

    /**
     * @param array $data
     * @return array
     */
    private function getTerminalsAction(array $data): array
    {
        $terminalRequestData = $data['terminal_request_data'];

        if (empty($terminalRequestData['sender_locality_id']) || empty($terminalRequestData['receiver_locality_id'])) {
            echo 'empty terminal data';
        }
        $requestParams = [
            'sender_locality_id' => $terminalRequestData['sender_locality_id'],
            'receiver_locality_id' => $terminalRequestData['receiver_locality_id'],
            'cargoes' => $terminalRequestData['cargoes'],
            'page' => $data['page'] ?: 1,
            'limit' => 600,
        ];
        if ($terminalRequestData['company']) {
            $requestParams['company'] = $terminalRequestData['company'];
        }

        if (isset($data['services_filter']) && !empty($data['services_filter'])) $requestParams['services_filter']=$data['services_filter'];

        $terminals = $this->getTerminals($requestParams);
        if (!isset($terminals['data'])) {
            echo json_encode(['error' => 'CANNOT GET TARIFF']);
            die();
        }
        return $terminals;
    }
    /**
     * Creating rate action
     * @param array $data
     * @return array
     * @throws Exception
     */
    private function createRateAction(array $data): array
    {
        if (empty($data['location']['term']) || empty($data['location']['iso'])) {
            echo json_encode(['error' => 'GEO DATA EMPTY']);
            die();
        }
        $senderParams = $data['sender_contact_data'];
		$senderParams['iso'] = 'RU';
		
        if (empty($senderParams['locality_id']) || empty($senderParams['zip'])) {
            if (empty($senderParams['cityFrom'])) {
                echo json_encode(['error' => 'WRONG SENDER CONTACT DATA']);
                die();
            }
            $senderLocationRequestData = [
                'term' => urldecode($senderParams['cityFrom']),
                'iso' => $senderParams['iso'],
                'limit' => 1,
            ];
            $geoSenderData = $this->getGeo($senderLocationRequestData)[0];
            if (empty($geoSenderData['id'])) {
                echo json_encode(['error' => 'NOT EXISTING SENDER ADDRESS']);
                die();
            }
            unset($senderParams['cityFrom']);
            $senderParams['locality_id'] = $geoSenderData['id'];
            $senderParams['zip'] = $geoSenderData['zip'];
        }

        $senderLocalityId = $senderParams["locality_id"];

        $locationRequestData = [
            'term' => urldecode($data['location']['term']),
            'iso' => $data['location']['iso'],
            'limit' => $data['location']['limit'] > 0 ? $data['location']['limit'] : 1,
        ];

        if ($data['location']['city_name']) {
            $locationRequestData['city_name'] = $data['location']['city_name'];
        }

        $geoData = $this->getGeo($locationRequestData)[0];

        $cargoes = [];

        if (is_array($data['cargo_data']) && is_array($data['cargo_data'][0])) {
            foreach ($data['cargo_data'] as $cargoParams) {
                $cargoData = $this->createCargo($this->filterCargo($cargoParams));
                if (empty($cargoData['id'])) {
                    echo json_encode(['error' => 'CANNOT CREATE CARGO']);
                    die();
                }
                $cargoes[] = $cargoData['id'];
            }
        } else {
            $cargoData = $this->createCargo($this->filterCargo($data['cargo_data']));
            if (empty($cargoData['id'])) {
                echo json_encode(['error' => 'CANNOT CREATE CARGO']);
                die();
            }
            $cargoes[] = $cargoData['id'];
        }


        $receiverLocalityId = $geoData['id'];


        $rateRequestData = [
            'receiver_locality_id' => $receiverLocalityId,
            'sender_locality_id' => $senderLocalityId,
            'cargoes' => $cargoes,
        ];
        $iconData = [];
        $iconResponse = $this->getIcons();

        foreach ($iconResponse as $item) {
            $iconData[$item['operator_id']] = $item;
        }

        if (isset($data['dadata_selected_choice']) && !empty($data['dadata_selected_choice']))
            $rateRequestData['dadata_selected_choice'] = $data['dadata_selected_choice'];

        $rateData = $this->createRate($rateRequestData);

        if (empty($rateData['key'])) {
            echo json_encode(['error' => 'CANNOT CREATE RATES']);
            die();
        }
        $rateData['locations'] = [
            'receiver_locality_id' => $receiverLocalityId,
            'sender_locality_id' => $senderLocalityId,
        ];
        $rateData['params'] = $rateRequestData;
        $rateData['icons'] = $iconData;
        return $rateData;
    }

    /**
     * Get existing rate action
     * @param array $data
     * @return array
     * @throws Exception
     */
    private function getRateAction(array $data): array
    {
        if (empty($data['rate_id']) || !is_string($data['rate_id'])) {
            throw new Exception('rate_id empty');
        }
        $rateId = $data['rate_id'];

        $filter = [
            'shipping_type_filter' => $data['shipping_type_filter'] ?: 'd2d',
        ];

        if ($data['pickup_days_shift'] > 0 && $data['pickup_days_shift'] <= 365) {
            $filter['pickup_days_shift'] = (int)$data['pickup_days_shift'];
        }
        if ($data['services_filter'] && in_array($data['services_filter'], ['NP', 'COD', 'NP,COD'])) {
            $filter['services_filter'] = $data['services_filter'];
        }
        if ($data['need_insurance'] === true) {
            $filter['need_insurance'] = true;
        }
        if ($data['insured_value']) {
            $filter['insured_value'] = $data['insured_value'];
        }

        $rate = $this->getRate($rateId, $filter);
        if (!isset($rate['results'])) {
            echo json_encode(['error' => 'CANNOT GET RATE']);
            die();
        }
        return $rate;
    }

    /**
     * @param $tariffId
     * @param null|array $filter
     * @return array
     */
    private function getTariff($tariffId, ?array $filter = null): array
    {
        return $this->get(
            $this->buildQueryString(
                self::TARIFF,
                $filter,
                $tariffId
            )
        );
    }
    private function getIcons(): array
    {
        return $this->get($this->buildQueryString(self::GET_ICONS));
    }
    private function getTerminal(string $terminalId): array
    {
        return $this->get(
            $this->buildQueryString(
                self::TERMINAL_GETTER_API,
                null,
                $terminalId
            )
        );
    }

    private function getTerminals(array $requestData): array
    {
        return $this->get($this->buildQueryString(
            self::TERMINAL_API,
            $requestData
        ));
    }

    /**
     * Getting rate
     * @param string $rateId
     * @param array $filter
     * @return array
     */
    private function getRate(string $rateId, array $filter): array
    {
        return $this->get(
            $this->buildQueryString(
                self::RATE_GETTER_API,
                $filter,
                $rateId
            )
        );
    }

    /**
     * Creating rate
     * @param array $rateParams
     * @return array
     */
    private function createRate(array $rateParams): array
    {
        return $this->post($this->buildQueryString(self::RATE_API), $rateParams);
    }

    /**
     * Creating contact
     * @param array $contactParams
     * @return array
     */
    private function createContact(array $contactParams): array
    {
        return $this->post($this->buildQueryString(self::CONTACT_API), $contactParams);
    }

    /**
     * Creation cargo
     * @param array $cargoParams
     * @return array
     */
    private function createCargo(array $cargoParams): array
    {
        return $this->post($this->buildQueryString(self::CARGO_API), $cargoParams);
    }

    /**
     * @param array $cargoParams
     * @param string $deliveryType
     * @return string[]
     */
    private function filterCargo(array $cargoParams, string $deliveryType = 'parcel'): array
    {
        $cargoSubDataNames = ['cargo_comment', 'height', 'length', 'width', 'quantity', 'weight'];

        $cargoRequestData = [
            'delivery_type' => $deliveryType,
        ];
        foreach ($cargoSubDataNames as $name) {
            if (isset($cargoParams[$name]) && !empty($cargoParams[$name])) {
                $cargoRequestData[$name] = $cargoParams[$name];

                switch ($name) {
                    case 'height':
                    case 'length':
                    case 'width':
                    $cargoRequestData[$name] = (int)(ceil($cargoRequestData[$name] / 10) ?: 1);
                        break;
                    case 'weight':
                        $cargoRequestData[$name] = $cargoRequestData[$name] / 1000;
                        break;
                }

            }
        }
        return $cargoRequestData;
    }
    /**
     * Get geo data
     * @param array $geoParams
     * @return array
     */
    private function getGeo(array $geoParams): array
    {
        return $this->get($this->buildQueryString(self::GEO_API, $geoParams));
    }

    /**
     * Building url for api request
     * @param string $apiMethod
     * @param array|null $queryParams
     * @param string $inlineParms
     * @return string
     */
    private function buildQueryString(string $apiMethod, ?array $queryParams = null, string $inlineParms = ''): string
    {
        $url = $this->apiUrl . $apiMethod . $inlineParms;
        $url.= $queryParams ? '?' . http_build_query($queryParams) : '';
        return $url;
    }

    /**
     * Curl get
     * @param string $url
     * @return array
     */
    private function get(string $url): array
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'X-Token: ' . $this->token,
                'Content-Type: application/json',
                'accept: application/json'
            ],
        ]);
        $response = curl_exec($curl);
        $responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($responseCode == 401) {
            echo json_encode(['error' => 'R401: Нет доступа.']);
            die();
        }
        if ($responseCode == 429) {
            echo json_encode(['error' => 'R429: Очень большое кол-во запросов.']);
            die();
        }
        if (empty($response)) {
            echo json_encode(['error' => curl_error($curl)]);
            die();
        }

        $result = json_decode($response, true);

        if (empty($result)) {
            echo json_encode(['error' => $response]);
            die();
        }

        curl_close($curl);
        return $result;
    }

    /**
     * curl post
     * @param string $url
     * @param array $paramsBody
     * @return array
     */
    private function post(string $url, array $paramsBody): array
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($paramsBody),
            CURLOPT_HTTPHEADER => [
                'X-Token: ' . $this->token,
                'Content-Type: application/json',
                'accept: application/json'
            ],
        ]);
        $response = curl_exec($curl);
        $responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($responseCode == 401) {
            echo json_encode(['error' => 'R401: Нет доступа.']);
            die();
        }
        if ($responseCode == 429) {
            echo json_encode(['error' => 'Code 429']);
            die();
        }
        if (empty($response)) {
            echo json_encode(['error' => curl_error($curl)]);
            die();
        }
        $result = json_decode($response, true);

        if (empty($result)) {
            echo json_encode(['error' => $response]);
            die();
        }

        curl_close($curl);
        return $result;
    }
}

$token = '9c78dab1bdad2407443ce568af6d64eac22584d52cd05ea53950fd8cccc6a1ec61e39a506684c60e438a4af323fc8a5d269443816e0863d852ee58925172e10b';
$apiUrl = 'https://eshop.catapulto.ru/';

$widgetApiHelper = new CatapultoWidgetData($token, $apiUrl);
$widgetApiHelper->deliveryWidgetAction($_POST);
